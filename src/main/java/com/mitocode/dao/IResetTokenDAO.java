package com.mitocode.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mitocode.model.ResetToken;

public interface IResetTokenDAO extends JpaRepository<ResetToken, Long>{

	ResetToken findByToken(String token);
}
